<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# sys_core_sh 0.3.7

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_sys_core_sh/develop?logo=python)](
    https://gitlab.com/ae-group/ae_sys_core_sh)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_sys_core_sh/release0.3.6?logo=python)](
    https://gitlab.com/ae-group/ae_sys_core_sh/-/tree/release0.3.6)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_sys_core_sh)](
    https://pypi.org/project/ae-sys-core-sh/#history)

>ae_sys_core_sh module 0.3.7.

[![Coverage](https://ae-group.gitlab.io/ae_sys_core_sh/coverage.svg)](
    https://ae-group.gitlab.io/ae_sys_core_sh/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_sys_core_sh/mypy.svg)](
    https://ae-group.gitlab.io/ae_sys_core_sh/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_sys_core_sh/pylint.svg)](
    https://ae-group.gitlab.io/ae_sys_core_sh/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_sys_core_sh)](
    https://gitlab.com/ae-group/ae_sys_core_sh/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_sys_core_sh)](
    https://gitlab.com/ae-group/ae_sys_core_sh/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_sys_core_sh)](
    https://gitlab.com/ae-group/ae_sys_core_sh/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_sys_core_sh)](
    https://pypi.org/project/ae-sys-core-sh/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_sys_core_sh)](
    https://gitlab.com/ae-group/ae_sys_core_sh/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_sys_core_sh)](
    https://libraries.io/pypi/ae-sys-core-sh)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_sys_core_sh)](
    https://pypi.org/project/ae-sys-core-sh/#files)


## installation


execute the following command to install the
ae.sys_core_sh module
in the currently active virtual environment:
 
```shell script
pip install ae-sys-core-sh
```

if you want to contribute to this portion then first fork
[the ae_sys_core_sh repository at GitLab](
https://gitlab.com/ae-group/ae_sys_core_sh "ae.sys_core_sh code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_sys_core_sh):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_sys_core_sh/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.sys_core_sh.html
"ae_sys_core_sh documentation").
